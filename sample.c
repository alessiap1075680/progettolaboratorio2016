#include <stdio.h>
#include <stdlib.h>



int main(int argc, char*argv[]) {
	if(argc < 2) {
		printf("Richiedo il nome del file\n");
		return -1;
	}

	printf("Apro %s\n\n", argv[1]);
	
	char* fin = (char*) calloc(200, sizeof(char));
	sprintf(fin, "stty -F %s sane raw pass8 -echo -hupcl cread 9600", argv[1]);
	system(fin);
	free(fin);
	
	/*fp contains a pointer to the file that contains the data captured by the sensor */
	FILE *fp;
	fp = fopen(argv[1], "r+");
	char c;
	
	FILE *doc;
	
	doc = fopen("data.txt", "a"); //a: Appends to a file. Writing operations, append data at the end of the file. The file is created if it does not exist.
	
	int k = 0;
	if(fp == NULL || doc == NULL) {
		printf("errore di I/O\n");
	} else {
	
		while(k<=60) {
			c = getc(fp);
			
			fprintf(doc, "%c", c);
			if(c == '\n'){
				k++;	
			}
		};


		fclose(fp);
		fclose(doc);
	}

	return 0;


}