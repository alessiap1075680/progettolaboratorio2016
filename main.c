/**
 * @file main.c
 * @brief Program that uses the library provided by process.c and process.h to analyze and dispaly the data captured by the DHT11 sensor
 *
 * @author Alessia Perissinotto (<alessia.perissinotto@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 */


/* libc include for the string functions */
#include <string.h>

#include <stdio.h>

#include <pthread.h>

#include <stdlib.h>

#include <string.h>

/* declares the functions provided by this library */
#include "process.h"





/**
 * @brief Program wich call the function estraiData() to save the data and the function menu() to analyze and display the data
 * @return The output of the functions that are called in menu()
 *
 * @author Alessia Perissinotto (<alessia.perissinotto@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 */


int main() {

    FILE *doc;

    int lines = countLines("/home/alessia/Documents/ProgettoLAB/temperature.txt");
    
    doc = fopen("/home/alessia/Documents/ProgettoLAB/temperature.txt", "r+");

    if (doc == NULL) {
        printf("errore di I/O\n");
    }

    else {

        char buffer[50];
        int i = 0;
        char ch;
        int t = 0;




        PTHT v[lines];


        for (int j = 0; j < lines; j++) {

            v[j] = (PTHT) malloc(sizeof (THT));

        }


        while (!feof(doc)) {

            ch = fgetc(doc);

            buffer[i++] = ch;

            if (ch == '\n') {

                estraiData(buffer, v[t++]);

                i = 0;

            }
        }

        menu(v, lines); 


        for (int j = 0; j < lines; j++) {
            free(v[j]);
        }


    }

    fclose(doc);

    return 0;


}
