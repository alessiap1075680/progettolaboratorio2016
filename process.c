/**
 * @file process.h
 * @brief Library for analyzing the data captured by the DTH11 sensor through the Arduino board
 *
 * @author Alessia Perissinotto (<alessia.perissinotto@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 */


/* libc include for the string functions */
#include <string.h>

#include <stdio.h>

#include <stdlib.h>

#include <string.h>

/* declares the functions provided by this library */
#include "process.h"


/*sets the size of the temporary buffer*/
#define BUFFER_LEN    (50)

/**
 * @brief Counts the number of lines in a text file
 *
 * @param filename the pointer to the first element of an array of char(a string). the name of the text file.
 *
 * @return a positive number which coincides with the number of lines in text; -1 if the file can't be opened.
 */


int countLines(char *filename) {

    FILE *fp = fopen(filename, "r");
    int ch;
    int lines = 0;

    if (fp == NULL)
        return -1;



    while (!feof(fp)) {
        ch = fgetc(fp);
        if (ch == '\n') {
            lines++;
        }
    }

    fclose(fp);
    return lines;


}

/**
 * @brief Takes the data from each line of the document and saves them in a struct THT.
 *
 * @param buff a pointer to the first element of an array of char. \c buff contains the part of the line already read.
 * @param data a pointer to a THT struct where the data will be saved.
 *
 * @return nothing.
 */

void estraiData(char *buff, PTHT data) {
    int i = 0;
    int len = BUFFER_LEN;
    char c; // current char
    char tmp[BUFFER_LEN]; // temporary string in stack
    int k = 0;

    memset(tmp, 0, BUFFER_LEN);

    while ((c = buff[i]) != ';' && i <= BUFFER_LEN) {
        tmp[k++] = c;
        i++;
    }

    data->hour = atoi(tmp);
    memset(tmp, 0, BUFFER_LEN);
    k = 0;
    i++;


    while ((c = buff[i]) != ';' && i <= BUFFER_LEN) {
        tmp[k++] = c;
        i++;
    }

    data->minutes = atoi(tmp);
    memset(tmp, 0, BUFFER_LEN);
    k = 0;
    i++;

    while ((c = buff[i]) != ';' && i <= BUFFER_LEN) {
        tmp[k++] = c;
        i++;
    }

    data->day = atoi(tmp);
    memset(tmp, 0, BUFFER_LEN);
    k = 0;
    i++;

    while ((c = buff[i]) != ';' && i <= BUFFER_LEN) {
        tmp[k++] = c;
        i++;
    }

    data->month = atoi(tmp);
    memset(tmp, 0, BUFFER_LEN);
    k = 0;
    i++;


    while ((c = buff[i]) != ';' && i <= BUFFER_LEN) {
        tmp[k++] = c;
        i++;
    }

    data->year = atoi(tmp);
    memset(tmp, 0, BUFFER_LEN);
    k = 0;
    i++;


    while ((c = buff[i]) != ';' && i <= BUFFER_LEN) {
        tmp[k++] = c;
        i++;
    }

    data->temperature = atof(tmp);
    memset(tmp, 0, BUFFER_LEN);
    k = 0;
    i++;

    while ((c = buff[i]) != '\n' && i <= BUFFER_LEN) {
        tmp[k++] = c;
        i++;
    }

    data->humidity = atof(tmp);
}

/**
 * @brief Print a list of all temperature values along with the date and the time of when the value has been captured.
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */


void printAllTemperature(PTHT *v, int size) {
 
    printf("All values of temperature are going to be printed ordered by date: \n");

    for (int i = 0; i < size; i++) {

      
        printf("%i/%i/%i %i:", v[i]->day, v[i]->month, v[i]->year, v[i]->hour);
        if ((v[i]-> minutes) < 10) {
            printf("0%i : ", v[i]-> minutes);
        } else {
            printf("%i : ", v[i]-> minutes);
        }
        printf("%.2f degree\n", v[i]-> temperature);
    }

}

/**
 * @brief Print a list of all humidity values along with the date and the time of when the value has been captured.
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printAllHumidity(PTHT *v, int size) {
    
    printf("All values of humidity are going to be printed ordered by date: \n");
    for (int i = 0; i < size; i++) {

        
        printf("%i/%i/%i %i:", v[i]->day, v[i]->month, v[i]->year, v[i]->hour);
        if ((v[i]-> minutes) < 10) {
            printf("0%i : ", v[i]-> minutes);
        } else {
            printf("%i : ", v[i]-> minutes);
        }
        printf("%.2f percent\n", v[i]-> humidity);
    }

}

/**
 * @brief  Print a list of all temperature values captured on a specific date(DD/MM/YYYY) that is entered by the user
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printAllTemperatureOfADate(PTHT *v, int size) {
    UINT32 d = 0;
    UINT32 m = 0;
    UINT32 y = 0;

    printf("Type the day without zeros ahead --> \t");
    scanf("%d", &d);
    printf("Type the month without zeros ahead --> \t");
    scanf("%d", &m);
    printf("Type the year without zeros ahead --> \t");
    scanf("%d", &y);


    printf("The entered date is %d/%d/%d\n", d, m, y);



    printf("In this date the values of temperature that have been recorded are:\n");
    for (int i = 0; i < size; i++) {
        if ((v[i]->day) == d && (v[i]->month) == m && (v[i]->year) == y) {
            printf("%d:", v[i]->hour);
            if((v[i]->minutes)<10){
                printf("0%d\t", v[i]->minutes);
            }
            else if((v[i]->minutes)>=10){
                printf("%d\t", v[i]->minutes);
            }
            printf("%.2f degree\n", v[i]-> temperature);

        }

    }
}

/**
 * @brief  Print a list of all humidity values captured on a specific date(DD/MM/YYYY) that is entered by the user
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printAllHumidityOfADate(PTHT *v, int size) {
    UINT32 d = 0;
    UINT32 m = 0;
    UINT32 y = 0;

    printf("Type the day without zeros ahead --> \t");
    scanf("%d", &d);
    printf("Type the month without zeros ahead --> \t");
    scanf("%d", &m);
    printf("Type the year without zeros ahead --> \t");
    scanf("%d", &y);


    printf("The entered date is %d/%d/%d\n", d, m, y);


    printf("In this date the values of humidity that have been recorded are:\n");
    for (int i = 0; i < size; i++) {
        if ((v[i]->day) == d && (v[i]->month) == m && (v[i]->year) == y) {
            printf("%d:%d\t", v[i]->hour, v[i]->minutes);
            printf("%.2f percent\n", v[i]-> humidity);

        }

    }
}

/**
 * @brief  Print the mean of the values of temperature recordered in a specific date insert by the user
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printMediumTemperatureOfADate(PTHT *v, int size) {

    int i = 0;
    double sum = 0;

    UINT32 d = 0;
    UINT32 m = 0;
    UINT32 y = 0;

    printf("Type the day without zeros ahead --> \t");
    scanf("%d", &d);
    printf("Type the month without zeros ahead --> \t");
    scanf("%d", &m);
    printf("Type the year without zeros ahead --> \t");
    scanf("%d", &y);


    printf("The entered date is %d/%d/%d\n", d, m, y);


    printf("The mean among the values of temperature captured on the entered date is:\t");
    for (int i = 0; i < size; i++) {
        if ((v[i]->day) == d && (v[i]->month) == m && (v[i]->year) == y) {
            sum = sum + (v[i]-> temperature);

        }
    }

    printf("%.2f\n", (sum / size));
}

/**
 * @brief  Print the mean of the values of humidity recordered in a specific date insert by the user
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printMediumHumidityOfADate(PTHT *v, int size) {


    int i = 0;
    double sum = 0;

    UINT32 d = 0;
    UINT32 m = 0;
    UINT32 y = 0;

    printf("Type the day without zeros ahead --> \t");
    scanf("%d", &d);
    printf("Type the month without zeros ahead --> \t");
    scanf("%d", &m);
    printf("Type the year without zeros ahead --> \t");
    scanf("%d", &y);


    printf("The entered date is %d/%d/%d\n", d, m, y);

    printf("The mean among the values of humidity captured on the entered date is:\t");
    for (int i = 0; i < size; i++) {
        if ((v[i]->day) == d && (v[i]->month) == m && (v[i]->year) == y) {
            sum = sum + (v[i]-> humidity);

        }
    }

    printf("%.2f\n", (sum / size));

}

/**
 * @brief  Print the date in which the highest temperature has been recorded
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printHottestDay(PTHT *v, int size) {

    int index = 0;
    double max = v[0]-> temperature;

    for (int c = 1; c < size; c++) {
        if ((v[c]->temperature) > max) {
            max = v[c]->temperature;
            index = c;
        }
    }

    printf("The hottest day was %d/%d/%d at %d:", v[index]-> day, v[index]-> month, v[index]-> year, v[index]-> hour);
    if ((v[index]-> minutes) < 10) {
        printf("0%i with a temperature of ", v[index]-> minutes);
    } else {
        printf("%i with a temperature of  ", v[index]-> minutes);
    }
    printf("%.2f degree\n", v[index]-> temperature);

}

/**
 * @brief  Print the date in which the lowest temperature has been recorded
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printColdestDay(PTHT *v, int size) {

    int index = 0;
    double min = v[0]-> temperature;

    for (int c = 1; c < size; c++) {
        if ((v[c]->temperature) < min) {
            min = v[c]->temperature;
            index = c;
        }
    }

    printf("The coldest day was %d/%d/%d at %d:", v[index]-> day, v[index]-> month, v[index]-> year, v[index]-> hour);
    if ((v[index]-> minutes) < 10) {
        printf("0%i with a temperature of ", v[index]-> minutes);
    } else {
        printf("%i with a temperature of  ", v[index]-> minutes);
    }
    printf("%.2f degree\n", v[index]-> temperature);


}

/**
 * @brief  Print the date in which the highest humidity has been recorded
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printWetterDay(PTHT *v, int size) {

    int index = 0;
    double max = v[0]-> humidity;

    for (int c = 1; c < size; c++) {
        if ((v[c]->humidity) > max) {
            max = v[c]->humidity;
            index = c;
        }
    }

    printf("The wetter day was %d/%d/%d at %d:", v[index]-> day, v[index]-> month, v[index]-> year, v[index]-> hour);
    if ((v[index]-> minutes) < 10) {
        printf("0%i with a humidity of ", v[index]-> minutes);
    } else {
        printf("%i with a temperature of  ", v[index]-> minutes);
    }
    printf("%.2f percent\n", v[index]-> humidity);

}

/**
 * @brief  Print the date in which the lowest humidity has been recorded
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printDriestDay(PTHT *v, int size) {

    int index = 0;
    double min = v[0]-> humidity;

    for (int c = 1; c < size; c++) {
        if ((v[c]->humidity) < min) {
            min = v[c]->humidity;
            index = c;
        }
    }

    printf("The driest day was %d/%d/%d at %d:", v[index]-> day, v[index]-> month, v[index]-> year, v[index]-> hour);
    if ((v[index]-> minutes) < 10) {
        printf("0%i with an humidity of ", v[index]-> minutes);
    } else {
        printf("%i with an humidity of  ", v[index]-> minutes);
    }
    printf("%.2f percent\n", v[index]-> temperature);

}

/**
 * @brief  Menu
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void menu(PTHT *v, int size) {
    int choice;
    char ex;
    do {
        system("clear"); //cleans the terminal screen
        printf("MAIN MENU\n");

        printf("Please choose an option from the following list by entering the number related to the function you want to play: \n");
        /* print a list of the avaible functions*/
        printf(" [1] Print all values of temperature  \n");
        printf(" [2] Print all values of humidity \n");
        printf(" [3] Print all values of temperature recorded in a specific date \n");
        printf(" [4] Print all values of humidity recorded in a specific date \n");
        printf(" [5] Print the mean of tempeature values recorded in a specific date \n");
        printf(" [6] Print the mean of humidity values recorded in a specific date \n");
        printf(" [7] Print the date of the hottest day \n");
        printf(" [8] Print the date of the coldest day \n");
        printf(" [9] Print the date of the wetter day \n");
        printf(" [10] Print the date of the driest day \n");


        printf("Make your choice: ");
        scanf("%d", &choice);

        
        
        switch (choice) {
            case 1:
                system("clear");
                printAllTemperature(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;
            case 2:
                system("clear");
                printAllHumidity(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;
            case 3:
                system("clear");
                printAllTemperatureOfADate(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;
            case 4:
                system("clear");
                printAllHumidityOfADate(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;
            case 5:
                system("clear");
                printMediumTemperatureOfADate(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;
            case 6:
                system("clear");
                printMediumHumidityOfADate(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;
            case 7:
                system("clear");
                printHottestDay(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;
            case 8:
                system("clear");
                printColdestDay(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;
            case 9:
                system("clear");
                printWetterDay(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;
            case 10:
                system("clear");
                printDriestDay(v, size);
                fflush(stdout);
                sleep(15);
                system("clear");
                break;

                /*Prima di questo commento è possibile inserire altre funzioni*/
                system("clear");
                break;
            default:
                system("clear");
                printDriestDay(v, size);
                printf("Not valid number \n");
                fflush(stdout);
                sleep(5);
                system("clear");
                break;
        }
        printf("Do you want to come back to main menu? \n");
        printf("[y] Yes, i want to come back to the main menu \n");
        printf("[n] No, i want to exit the menu \n");
        printf("Make your choice: ");
        scanf("%s", &ex);
    } while (ex != 'n');
    
    printf("Goodbye!!\n");
}
