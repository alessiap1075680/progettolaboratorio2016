/**
 * @file process.h
 * @brief Library for analyzing the data captured by the DTH11 sensor through the Arduino board
 *
 * @author Alessia Perissinotto (<alessia.perissinotto@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 */


#ifndef PROCESS_H

#define PROCESS_H

/* libc include for the standard definitions */
#include <stddef.h>

/*declares a new type for unsigned int*/
typedef unsigned int UINT32;

/*The structure THT(Time-Humidity-Temperature) contains the parameter used to describe the data that have been captured by the sensor*/

typedef struct {
    UINT32 year;
    UINT32 month;
    UINT32 day;
    UINT32 hour;
    UINT32 minutes;
    double humidity;
    double temperature;

} THT, *PTHT;

/**
 * @brief Counts the number of lines in a text file
 *
 * @param filename the pointer to the first element of an array of char(a string). the name of the text file.
 *
 * @return a positive number which coincides with the number of lines in text; -1 if the file can't be opened.
 */

int countLines(char *filename);


/**
 * @brief Takes the data from each line of the document and saves them in a struct THT.
 *
 * @param buff a pointer to the first element of an array of char. \c buff contains the part of the line already read.
 * @param data a pointer to a THT struct where the data will be saved.
 *
 * @return nothing.
 */

void estraiData(char *buff, PTHT data);

/**
 * @brief Print a list of all temperature values along with the date and the time of when the value has been captured.
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */


void printAllTemperature(PTHT *v, int size);

/**
 * @brief Print a list of all humidity values along with the date and the time of when the value has been captured.
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printAllHumidity(PTHT *v, int size);

/**
 * @brief  Print a list of all temperature values captured on a specific date(DD/MM/YYYY) that is entered by the user
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */
void printAllTemperatureOfADate(PTHT *v, int size);

/**
 * @brief  Print a list of all humidity values captured on a specific date(DD/MM/YYYY) that is entered by the user
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printAllHumidityOfADate(PTHT *v, int size);

/**
 * @brief  Print the mean of the values of temperature recordered in a specific date insert by the user
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printMediumTemperatureOfADate(PTHT *v, int size);

/**
 * @brief  Print the mean of the values of humidity recordered in a specific date insert by the user
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printMediumHumidityOfADate(PTHT *v, int size);

/**
 * @brief  Print the date in which the highest temperature has been recorded
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printHottestDay(PTHT *v, int size);

/**
 * @brief  Print the date in which the lowest temperature has been recorded
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printColdestDay(PTHT *v, int size);

/**
 * @brief  Print the date in which the highest humidity has been recorded
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printWetterDay(PTHT *v, int size);

/**
 * @brief  Print the date in which the lowest humidity has been recorded
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void printDriestDay(PTHT *v, int size);

/**
 * @brief  Menu
 *
 * @param v a pointer to an array of pointers to struct.
 * @param size an integer that defines the array size.
 *
 * @return nothing.
 */

void menu(PTHT *v, int size);


#endif