/**
 * @file sketcharduino
 * @brief Raccoglie dati relativi a temperatura e umidità dal sensore digitale DHT11 e li stampa sia sulla porta seriale che su un display lcd
 *
 * @author Alessia Perissinotto (<alessia.perissinotto@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * 
 */

/*Libreria inclusa per le funzioni relative al sensore */
#include <dht.h>

/*Libreria inclusa per le funzioni relative allo schermo lcd */
#include <LiquidCrystal.h>

/*Definisce il pin dell'arduino al quale è stato collegato l'out del DHT11 sensor */
#define DHT11_PIN 14


/*Inizializza i pin dell'arduino ai quali sono state collegate le uscite dello schermo */
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

dht DHT;
/*Inizializza un vettore di unsigned int che contenga i 60 valori di temperatura misurati ogni secondo in un minuto  */
unsigned int VT[60];
/*Inizializza un vettore di unsigned int che contenga i 60 valori di umidità misurati ogni secondo in un minuto  */
unsigned int VH[60];
/*Inizializza l'indice del vettore VT */
int i = 0;
/*Inizializza l'indice del vettore VT */
int k =0;


/*Inizializza i parametri dell'orologio-calendario*/
unsigned int months[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 31, 31 };

/*Anno, mese, giorno, ora, minuti e secondi devono essere impostati prima di avviare il programma e il campionamento dei dati */
unsigned int year = 2016;

unsigned int  month = 6;

unsigned int   day = 8;

unsigned int  hour = 16;

unsigned int   min = 0;

unsigned int  sec = 0;

unsigned int dayofweek = 0;

unsigned int bHour = 0, bisestile = 0;

/*Il metodo setup() esegue le istruzioni contenute nel suo corpo una sola volta quando viene avviato il programma */
void setup() {
        
        //Imposta il rate di trasmissione dei dati verso la porta seriale a 9600  bits per secondo (baud)
	Serial.begin(9600);
	//Imposta le dimensioni dello schermo lcd: larghezza =16 X altezza = 2
        lcd.begin(16, 2);
	
        dayofweek = wday(year, month - 1, day);

	lcd.print("Valori di temperatura e umidità verranno stampati ogni minuto");
	//Serial.println("avvio");
	delay(2000);
	//Resetta lo schermo del display
        lcd.clear();
}

int wday(unsigned char year, unsigned char month, unsigned char day) {
	unsigned char adjustment, mm, yy;
	adjustment = (14 - month) / 12;
	mm = month + 12 * adjustment - 2;
	yy = year - adjustment;

	return (day + (13 * mm - 1) / 5 + yy + yy / 4 - yy / 100 + yy / 400) % 7;
}

/*Stampa ogni minuto i valori sulla porta seriale e sul display i valori di umidità e temperatura ottenuti dalla media dei 60 valori campionati ogni secondo e salvati temporaneamente 
nei vettori VT e VH*/
void Invia() {
	lcd.clear();

        //Inizializzo i valori delle medie come numeri double
        double mt = 0;
        double mh = 0;
        
        for(int j = 0; j < 60; j++) {
          mt += VT [j];
        }
        
        //Calcola la media dei valori di temperatura e resetta a 0 l'indice 
        mt /= 60;
        i = 0;
        
         for(int j = 0; j < 60; j++) {
          mh += VH [j];
        }
        
        //Calcola la media dei valori di temperatura e resetta a 0 l'indice 
        mh /=60;
        k = 0;


	Serial.print(hour);
       
        Serial.print(';');
       
        Serial.print(min);
       
        Serial.print(';');
       
        Serial.print(day);
       
        Serial.print(';');
       
        Serial.print(month);
       
        Serial.print(';');
       
        Serial.print(year);
       
        Serial.print(';');
       
        Serial.print(mt);
       
        Serial.print(';');
	
        Serial.print(mh);       
        
        Serial.println(';');
	
        //Imposta la posizone sul display dove cominciare a scrivere 
        lcd.setCursor(0, 0);
        
        lcd.print("T=");
	 
        lcd.print(mt);
	
        lcd.setCursor(9, 0);
        
        lcd.print("U=");
	
        lcd.print(mh);
        
        lcd.setCursor(0, 1);
        
        lcd.print(hour);
        
        lcd.print(':');
        
        lcd.print(min);
        
        lcd.print(' ');
        
        lcd.print(day);
        
        lcd.print('/');
        
        lcd.print(month);
        
        lcd.print('/');
        
        lcd.print(year);
        
}

void Campiona() {
	int a = DHT.read11(DHT11_PIN);
        
        //campiona i valori di temperatura e li salva ne vettore VT
        VT[i++]=DHT.temperature;
         //campiona i valori di umidità e li salva ne vettore VH      
        VH[k++]=DHT.humidity;
        
        
}


/*Il metodo loop() esegue ciclicamente le istruzioni contenute nel suo corpo come in un loop infinito da quando viene avviato il programma */
void loop() {
	
        //imposto il ritardo in millisecondi che il programma aspetta ogni volta che inizia il loop()
        delay(1000);
	
        sec++;
	
        //Campiono i dati e li metto nel vettore
        Campiona(); 

	if (sec >= 60) {
		min++;
		sec = 0;

		Invia();

		//elaboro i dati e li invio allo schermo e al pc
	}

	
        if (min >= 60) {
		min = 0;
		hour++;
		bHour = 1;
	}

	
        if (hour >= 24) {
		hour = 0;
	}

	
         if (bHour) {
		bHour = 0;

		if ((month == 2) && (dayofweek == 0)) {
			if (day > ((months[month - 1])) - 7) {

				if (hour == 2) {
					hour++;
				}
			}
		}
		else if ((month == 10) && (dayofweek == 0)) {
			if (day > ((months[month - 1])) - 7) {
				if (hour == 3) {
					hour--;
				}
			}
		}

		if ((hour == 0) && min == 0) {
			if (++dayofweek > 6) {
				dayofweek = 0;
			}

			if (++day > (months[month - 1] + ((month == 2) ? bisestile : 0))) {
				day = 1;

				if (++month > 12) {
					month = 1;
					year++;

					bisestile = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) ? 1 : 0;
				}
			}
		}
	}
}
